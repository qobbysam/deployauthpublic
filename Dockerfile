FROM alpine:3.13
#FROM golang:alpine AS build
#RUN apk --no-cache add gcc g++ make git
#WORKDIR .
#COPY . .
# RUN go mod init webserver
# RUN go mod tidy
# RUN GOOS=linux go build -ldflags="-s -w" -o ./bin/web-app ./main.go

#FROM alpine:3.13
#RUN apk --no-cache add ca-certificates
#WORKDIR /usr/bin
COPY .  /usr/src/app

#CMD ./deploy/main
#WORKDIR /usr/bin
#COPY --from=base /go/src/app/bin /go/bin
#EXPOSE 6999
#EXPOSE 6888
#EXPOSE 6777
#ENTRYPOINT /usr/bin/bidderdeploy/deploy/main --port 80
